#######################################
#jun 18 2021
#attempt to clean run 'detectron2_inference_only' notebook from modelding team
#with model_final.pth custom file; some commonality and some uniqueness, 
#namely the model_final.pth file from modeling team, and no visualization her
#using video_53 samples folder and a subset of it (test53) to test pngs.
#output is # of humans detected per image
#june12 update - jetson ran 20+ images, and correctly detected humans in some!  maxing at 13 images at a time
#due to temperature rise
#######################################
# import some common libraries
import cv2
import numpy as np
import requests, sys, time, os
from PIL import Image
import os
import glob
#######################################
# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.data.datasets import register_coco_instances
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetMapper, DatasetCatalog, MetadataCatalog, build_detection_test_loader
###################################################

#register_coco_instances("wildlife_dataset_test", {}, "./test53/annotations/instances_default.json", "./test53/images")
register_coco_instances("wildlife_dataset_test", {}, "./video_53_sample_1/annotations/instances_default.json", "./test53/images")

#configuration
print("initializing defaults...")
cfg = get_cfg()
#print(cfg)
# add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library
cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"))
cfg.DATALOADER.NUM_WORKERS = 4
#setting up the initial weights
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml")  
# Let training initialize from model zoo
print("done.")
time.sleep(1)

# Solver options
print("loading custom solver...")
cfg.SOLVER.BASE_LR = 0.001 # Base learning rate
cfg.SOLVER.IMS_PER_BATCH = 1  # Lower to reduce memory usage (1 is the lowest)
cfg.SOLVER.WARMUP_ITERS = 100  # Warmup iterations to linearly ramp learning rate from zero
cfg.SOLVER.MAX_ITER = 10  # Maximum number of iterations \
# Adjust up if val mAP is still rising, adjust down if overfit
cfg.SOLVER.STEPS = (100, 200)  # Iterations at which to decay learning rate
cfg.SOLVER.GAMMA = 0.5  # Learning rate decay # previous 0.05
print("done.")
time.sleep(1)
print("setting regions of interest...")
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 64
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 2
cfg.TEST.EVAL_PERIOD = 20  # evaluation once after 20 iterations on the cfg.DATASETS.TEST, which is the Validation set
print("done.")
time.sleep(1)

print("loading custom model...")
#setting up the weights with modeling teams file or use default
#cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml")  # Let training initialize from model zoo
cfg.MODEL.WEIGHTS = ("model_final.pth")
print("done.")
time.sleep(1)

#defaulted to 0.5, changed to 0.91 (from modeling team)
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.91  # set threshold for this model (0.91 from modeling team)
print("threshhold set to: " , cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST)
time.sleep(1)

#setting up output (not applicable?)
#output_path = cfg.OUTPUT_DIR
#print("Files in output directory:::", os.listdir(output_path))

#setting up predictor
print("loading default predictor...")
test_metadata = MetadataCatalog.get("wildlife_dataset_test")
predictor = DefaultPredictor(cfg)
counter = 0
instances=[]
print("done.")
time.sleep(1)

#######running inference on acutal looped data
print("starting inference on test data...")
#for imageName in glob.glob("./test53/images" + '/*.PNG'):
for imageName in glob.glob("./video_53_sample_1/images" + '/*86.PNG'):  #enter a 2-dig numb after *, 00-99
    counter = counter + 1
    im = cv2.imread(imageName)
    print('image ', counter, 'loaded')
    outputs = predictor(im)
    v = Visualizer(im[:, :, ::-1],
                   metadata=test_metadata,
                   scale=0.8
                   )
    l = len(outputs["instances"])
    instances.append(l)
    print("infer ", counter, "done")
    time.sleep(1)
print("Number of test images:", len(instances))
print("Number of Humans in each images:", instances)
