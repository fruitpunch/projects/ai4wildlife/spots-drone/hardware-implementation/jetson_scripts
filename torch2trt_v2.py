#script to load pretrained mobilenetv2 model from pytorch to nano, 
#convert to tensorrto on nano, and run infrerences with both on nano
#requires 1 add'l file:  'imagenet-simple-labels.json'
#also can download any test images from web (a few here in this directory for test)
#[no visualizations included]
#[no timing test comparisons inlcuded]

#jun 6, 2021
#this script works if running in the 'torch2trt' folder where dependencies were installed in environment subfolder
#you may or may not need to activate the env

#import libraries
import torch, json
import numpy as np
import tensorrt as trt
from torchvision import datasets, models, transforms
from torch2trt import torch2trt
from PIL import Image

#load pretrained model
model = models.mobilenet_v2(pretrained=True)
# Send the model to the GPU 
model.cuda()
# Set layers such as dropout and batchnorm in evaluation mode
model.eval();

with open("imagenet-simple-labels.json") as f:
    labels = json.load(f)

data_transform = transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor()])

#test_image = 'elephant.jpeg'
#test_image = 'elephant1.jpg'
#test_image = 'elephant2.jpg'
#test_image = 'elephant3.jpg' #cartoon elephant
#test_image = 'toucan.jpeg'
test_image = 'me2.jpg'

image = Image.open(test_image)

image = data_transform(image).unsqueeze(0).cuda()

# create example data 
x = torch.ones((1, 3, 224, 224)).cuda()

# convert to TensorRT feeding sample data as input
model_trt = torch2trt(model, [x])


out = model(image)
out_trt = model_trt(image)
# Find the predicted class
print("Predicted model_torch class is: {}".format(labels[out.argmax()]))
print("Predicted model_trt class is: {}".format(labels[out_trt.argmax()]))